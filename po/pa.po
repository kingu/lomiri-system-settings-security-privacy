# Punjabi translation for lomiri-system-settings-security-privacy
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-system-settings-security-privacy package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
# Gursharnjit Singh <ubuntuser13@gmail.com>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-security-privacy\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-06 13:32+0000\n"
"PO-Revision-Date: 2015-03-11 04:49+0000\n"
"Last-Translator: Gursharnjit_Singh <ubuntuser13@gmail.com>\n"
"Language-Team: pa-l10n  Owner:Amanpreet Alam\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2015-07-16 05:41+0000\n"
"X-Generator: Launchpad (build 17628)\n"

#: ../plugins/security-privacy/AppAccess.qml:29
#: ../plugins/security-privacy/AppAccess.qml:30
#: ../plugins/security-privacy/PageComponent.qml:247
#: ../plugins/security-privacy/PageComponent.qml:248
#, fuzzy
msgid "App permissions"
msgstr "ਅਧਿਕਾਰ"

#: ../plugins/security-privacy/AppAccess.qml:69
#: ../plugins/security-privacy/AppAccess.qml:70
#, fuzzy
msgid "Apps that you have granted access to:"
msgstr "ਐਪ ਜਿਹਨਾਂ ਨੂੰ ਤੁਸੀਂ ਪ੍ਰਵਾਨਿਤ ਕੀਤਾ ਅਤੇ ਲਈ ਪਹੁੰਚ ਦੀ ਬੇਨਤੀ ਕੀਤੀ:"

#: ../plugins/security-privacy/AppAccess.qml:75
#: ../plugins/security-privacy/AppAccess.qml:76
msgid "Camera"
msgstr "ਕੈਮਰਾ"

#: ../plugins/security-privacy/AppAccess.qml:76
#: ../plugins/security-privacy/AppAccess.qml:77
msgid "Apps that have requested access to your camera"
msgstr "ਐਪ ਜਿਹਨਾਂ ਨੇ ਤੁਹਾਡੇ ਕੈਮਰੇ ਲਈ ਪਹੁੰਚ ਦੀ ਬੇਨਤੀ ਕੀਤੀ"

#: ../plugins/security-privacy/AppAccess.qml:81
#: ../plugins/security-privacy/AppAccess.qml:82
#: ../plugins/security-privacy/Location.qml:34
#: ../plugins/security-privacy/Location.qml:35
#: ../plugins/security-privacy/PageComponent.qml:228
#: ../plugins/security-privacy/PageComponent.qml:229
msgid "Location"
msgstr "ਟਿਕਾਣਾ"

#: ../plugins/security-privacy/AppAccess.qml:82
#: ../plugins/security-privacy/AppAccess.qml:83
#, fuzzy
msgid "Apps that have requested access to your location"
msgstr "ਐਪ ਜਿਹਨਾਂ ਨੇ ਤੁਹਾਡੇ ਮਾਈਕ ਲਈ ਪਹੁੰਚ ਦੀ ਬੇਨਤੀ ਕੀਤੀ"

#: ../plugins/security-privacy/AppAccess.qml:87
#: ../plugins/security-privacy/AppAccess.qml:88
#, fuzzy
msgid "Microphone"
msgstr "ਫ਼ੋਨ"

#: ../plugins/security-privacy/AppAccess.qml:88
#: ../plugins/security-privacy/AppAccess.qml:89
#, fuzzy
msgid "Apps that have requested access to your microphone"
msgstr "ਐਪ ਜਿਹਨਾਂ ਨੇ ਤੁਹਾਡੇ ਮਾਈਕ ਲਈ ਪਹੁੰਚ ਦੀ ਬੇਨਤੀ ਕੀਤੀ"

#: ../plugins/security-privacy/AppAccess.qml:102
#: ../plugins/security-privacy/AppAccess.qml:103
#, qt-format
msgid "%1/%2"
msgstr "%1/%2"

#: ../plugins/security-privacy/AppAccess.qml:103
#: ../plugins/security-privacy/AppAccess.qml:104
msgid "0"
msgstr "0"

#: ../plugins/security-privacy/AppAccess.qml:118
#: ../plugins/security-privacy/AppAccess.qml:119
#, fuzzy
msgid "Apps may also request access to online accounts."
msgstr "ਐਪ ਜਿਹਨਾਂ ਨੇ ਤੁਹਾਡੇ ਮਾਈਕ ਲਈ ਪਹੁੰਚ ਦੀ ਬੇਨਤੀ ਕੀਤੀ"

#: ../plugins/security-privacy/AppAccess.qml:123
#: ../plugins/security-privacy/AppAccess.qml:124
msgid "Online Accounts…"
msgstr ""

#: ../plugins/security-privacy/Location.qml:96
#: ../plugins/security-privacy/Location.qml:97
#, fuzzy
msgid "Let the device detect your location:"
msgstr "ਐਪ ਜਿਹਨਾਂ ਨੇ ਤੁਹਾਡੇ ਮਾਈਕ ਲਈ ਪਹੁੰਚ ਦੀ ਬੇਨਤੀ ਕੀਤੀ"

#: ../plugins/security-privacy/Location.qml:175
#: ../plugins/security-privacy/Location.qml:176
msgid "Using GPS only (less accurate)"
msgstr ""

#: ../plugins/security-privacy/Location.qml:176
#: ../plugins/security-privacy/Location.qml:177
msgid "Using GPS"
msgstr ""

#: ../plugins/security-privacy/Location.qml:188
#: ../plugins/security-privacy/Location.qml:189
#, qt-format
msgid ""
"Using GPS, anonymized Wi-Fi and cellular network info.<br>By selecting this "
"option you accept the <a href='%1'>Nokia HERE terms and conditions</a>."
msgstr ""

#: ../plugins/security-privacy/Location.qml:189
#: ../plugins/security-privacy/Location.qml:190
#, qt-format
msgid ""
"Using GPS and anonymized Wi-Fi info.<br>By selecting this option you accept "
"the <a href='%1'>Nokia HERE terms and conditions</a>."
msgstr ""

#: ../plugins/security-privacy/Location.qml:195
#: ../plugins/security-privacy/Location.qml:196
msgid "Not at all"
msgstr ""

#: ../plugins/security-privacy/Location.qml:212
#: ../plugins/security-privacy/Location.qml:213
msgid ""
"Uses GPS to detect your rough location. When off, GPS turns off to save "
"battery."
msgstr ""
"ਤੁਹਾਡੀ ਸਥਿਤੀ ਜਾਨਣ ਲਈ ਜੀਪੀਐਸ ਦੀ ਵਰਤੋਂ ਕਰਦਾ। ਜਦੋਂ ਬੰਦ ਹੋਵੇ, ਬੈਟਰੀ ਬਚਾਉਣ ਲਈ GPS ਬੰਦ ਕਰੋ।"

#: ../plugins/security-privacy/Location.qml:214
#: ../plugins/security-privacy/Location.qml:215
msgid ""
"Uses WiFi and GPS to detect your rough location. Turning off location "
"detection saves battery."
msgstr ""
"ਤੁਹਾਡੀ ਸਥਿਤੀ ਜਾਨਣ ਲਈ ਵਾਈ-ਫ਼ਾਈ ਅਤੇ ਜੀਪੀਐਸ ਦੀ ਵਰਤੋਂ ਕਰਦਾ। ਸਥਿਤੀ ਖੋਜ ਬੰਦ ਕਰਨਾ ਬੈਟਰੀ "
"ਬਚਾਉਦਾ।"

#: ../plugins/security-privacy/Location.qml:216
#: ../plugins/security-privacy/Location.qml:217
msgid ""
"Uses WiFi (currently off) and GPS to detect your rough location. Turning off "
"location detection saves battery."
msgstr ""
"ਤੁਹਾਡੀ ਸਥਿਤੀ ਜਾਨਣ ਲਈ ਵਾਈ-ਫ਼ਾਈ (ਇਸ ਵੇਲੇ ਬੰਦ) ਅਤੇ ਜੀਪੀਐਸ ਦੀ ਵਰਤੋਂ ਕਰਦਾ। ਸਥਿਤੀ ਖੋਜ ਬੰਦ "
"ਕਰਨਾ ਬੈਟਰੀ ਦੀ ਬਚਾਉਦਾ।"

#: ../plugins/security-privacy/Location.qml:218
#: ../plugins/security-privacy/Location.qml:219
msgid ""
"Uses WiFi, cell tower locations, and GPS to detect your rough location. "
"Turning off location detection saves battery."
msgstr ""
"ਤੁਹਾਡੀ ਸਥਿਤੀ ਜਾਨਣ ਲਈ ਵਾਈ-ਫ਼ਾਈ, ਸੈੱਲ ਟਾਵਰ ਸਥਿਤੀਆਂ, ਅਤੇ ਜੀਪੀਐਸ ਦੀ ਵਰਤੋਂ ਕਰਦਾ। ਸਥਿਤੀ ਖੋਜ "
"ਬੰਦ ਕਰਨਾ ਬੈਟਰੀ ਬਚਾਉਦਾ।"

#: ../plugins/security-privacy/Location.qml:220
#: ../plugins/security-privacy/Location.qml:221
msgid ""
"Uses WiFi, cell tower locations (no current cellular connection), and GPS to "
"detect your rough location. Turning off location detection saves battery."
msgstr ""
"ਤੁਹਾਡੀ ਸਥਿਤੀ ਜਾਨਣ ਲਈ ਵਾਈ-ਫ਼ਾਈ, ਸੈੱਲ ਟਾਵਰ ਸਥਿਤੀਆਂ (ਇਸ ਵੇਲੇ ਕੋਈ ਸੈਲੂਲਰ ਕੁਨੇਕਸ਼ਨ ਨਹੀਂ), ਅਤੇ "
"ਜੀਪੀਐਸ ਦੀ ਵਰਤੋਂ ਕਰਦਾ। ਸਥਿਤੀ ਖੋਜ ਬੰਦ ਕਰਨਾ ਬੈਟਰੀ ਬਚਾਉਦਾ।"

#: ../plugins/security-privacy/Location.qml:222
#: ../plugins/security-privacy/Location.qml:223
msgid ""
"Uses WiFi (currently off), cell tower locations, and GPS to detect your "
"rough location. Turning off location detection saves battery."
msgstr ""
"ਤੁਹਾਡੀ ਸਥਿਤੀ ਜਾਨਣ ਲਈ ਵਾਈ-ਫ਼ਾਈ (ਇਸ ਵੇਲੇ ਬੰਦ), ਸੈਲ ਟਾਵਰ ਸਥਿਤੀਆਂ, ਅਤੇ ਜੀਪੀਐਸ ਦੀ ਵਰਤੋਂ "
"ਕਰਦਾ। ਸਥਿਤੀ ਖੋਜ ਬੰਦ ਕਰਨਾ ਬੈਟਰੀ ਬਚਾਉਦਾ।"

#: ../plugins/security-privacy/Location.qml:224
#: ../plugins/security-privacy/Location.qml:225
msgid ""
"Uses WiFi (currently off), cell tower locations (no current cellular "
"connection), and GPS to detect your rough location. Turning off location "
"detection saves battery."
msgstr ""
"ਤੁਹਾਡੀ ਸਥਿਤੀ ਜਾਨਣ ਲਈ ਵਾਈ-ਫ਼ਾਈ (ਇਸ ਵੇਲੇ ਬੰਦ), ਸੈਲ ਟਾਵਰ ਸਥਿਤੀਆਂ (ਕੋਈ ਮੌਜੂਦਾ ਸੈਲੂਲਰ ਕੁਨੇਕਸ਼ਨ "
"ਨਹੀਂ), ਅਤੇ ਜੀਪੀਐਸ ਦੀ ਵਰਤੋਂ ਕਰਦਾ। ਸਥਿਤੀ ਖੋਜ ਬੰਦ ਕਰਨਾ ਬੈਟਰੀ ਬਚਾਉਦਾ।"

#: ../plugins/security-privacy/Location.qml:231
#: ../plugins/security-privacy/Location.qml:232
#, fuzzy
msgid "Let apps access this location:"
msgstr "ਟਿਕਾਣੇ ਤੇ ਪਹੁੰਚ ਦੀ ਪ੍ਰਵਾਨਗੀ ਦਿਓ:"

#: ../plugins/security-privacy/Location.qml:254
#: ../plugins/security-privacy/Location.qml:255
#, fuzzy
msgid "None requested"
msgstr "ਕੋਈ ਨਹੀਂ ਮਿਲਿਆ"

#: ../plugins/security-privacy/LockSecurity.qml:36
#: ../plugins/security-privacy/LockSecurity.qml:37
#: ../plugins/security-privacy/PhoneLocking.qml:66
#: ../plugins/security-privacy/PhoneLocking.qml:67
msgid "Lock security"
msgstr "ਲਾਕ ਸੁਰੱਖਿਆ"

#: ../plugins/security-privacy/LockSecurity.qml:137
#: ../plugins/security-privacy/LockSecurity.qml:138
#: ../plugins/security-privacy/LockSecurity.qml:563
#: ../plugins/security-privacy/LockSecurity.qml:564
msgid "Change passcode…"
msgstr "ਪਾਸਕੋਡ ਬਦਲੋ..."

#: ../plugins/security-privacy/LockSecurity.qml:139
#: ../plugins/security-privacy/LockSecurity.qml:140
#: ../plugins/security-privacy/LockSecurity.qml:564
#: ../plugins/security-privacy/LockSecurity.qml:565
msgid "Change passphrase…"
msgstr "ਪਾਸਫ਼ਰੇਜ਼ ਬਦਲੋ..."

#: ../plugins/security-privacy/LockSecurity.qml:146
#: ../plugins/security-privacy/LockSecurity.qml:147
msgid "Switch to swipe"
msgstr "ਸਵਾਈਪ ਤੇ ਸਵਿੱਚ ਕਰੋ"

#: ../plugins/security-privacy/LockSecurity.qml:148
#: ../plugins/security-privacy/LockSecurity.qml:149
msgid "Switch to passcode"
msgstr "ਪਾਸਕੋਡ ਤੇ ਸਵਿੱਚ ਕਰੋ"

#: ../plugins/security-privacy/LockSecurity.qml:150
#: ../plugins/security-privacy/LockSecurity.qml:151
msgid "Switch to passphrase"
msgstr "ਪਾਸਫ਼ਰੇਜ਼ ਤੇ ਸਵਿੱਚ ਕਰੋ"

#: ../plugins/security-privacy/LockSecurity.qml:159
#: ../plugins/security-privacy/LockSecurity.qml:160
msgid "Existing passcode"
msgstr "ਮੌਜੂਦਾ ਪਾਸਕੋਡ"

#: ../plugins/security-privacy/LockSecurity.qml:161
#: ../plugins/security-privacy/LockSecurity.qml:162
msgid "Existing passphrase"
msgstr "ਮੌਜੂਦਾ ਪਾਸਪਰੇਜ"

#: ../plugins/security-privacy/LockSecurity.qml:231
#: ../plugins/security-privacy/LockSecurity.qml:232
msgid "Choose passcode"
msgstr "ਪਾਸਕੋਡ ਚੁਣੋ"

#: ../plugins/security-privacy/LockSecurity.qml:233
#: ../plugins/security-privacy/LockSecurity.qml:234
msgid "Choose passphrase"
msgstr "ਪਾਸਪਰੇਜ ਚੁਣੋ"

#: ../plugins/security-privacy/LockSecurity.qml:291
#: ../plugins/security-privacy/LockSecurity.qml:292
msgid "Confirm passcode"
msgstr "ਪਾਸਕੋਡ ਦੀ ਪੁਸ਼ਟੀ ਕਰੋ"

#: ../plugins/security-privacy/LockSecurity.qml:293
#: ../plugins/security-privacy/LockSecurity.qml:294
msgid "Confirm passphrase"
msgstr "ਪਾਸਪਰੇਜ ਦੀ ਪੁਸ਼ਟੀ ਕਰੋ"

#: ../plugins/security-privacy/LockSecurity.qml:348
#: ../plugins/security-privacy/LockSecurity.qml:349
msgid "Those passcodes don't match. Try again."
msgstr "ਇਹਨਾਂ ਪਾਸਕੋਡਾਂ ਦਾ ਮੇਲ ਨਹੀਂ। ਮੁੜ-ਕੋਸ਼ਿਸ ਕਰੋ।"

#: ../plugins/security-privacy/LockSecurity.qml:351
#: ../plugins/security-privacy/LockSecurity.qml:352
msgid "Those passphrases don't match. Try again."
msgstr "ਇਹਨਾਂ ਪਾਸਪਰੇਜਾਂ ਦਾ ਮੇਲ ਨਹੀਂ। ਮੁੜ-ਕੋਸ਼ਿਸ ਕਰੋ।"

#: ../plugins/security-privacy/LockSecurity.qml:365
#: ../plugins/security-privacy/LockSecurity.qml:366
#: ../plugins/security-privacy/SimPin.qml:179
#: ../plugins/security-privacy/SimPin.qml:318
msgid "Cancel"
msgstr "ਰੱਦ"

#: ../plugins/security-privacy/LockSecurity.qml:386
#: ../plugins/security-privacy/LockSecurity.qml:387
msgid "Unset"
msgstr "ਬਿਨਾਂ-ਸੈੱਟ"

#: ../plugins/security-privacy/LockSecurity.qml:389
#: ../plugins/security-privacy/LockSecurity.qml:390
#: ../plugins/security-privacy/SimPin.qml:187
msgid "Change"
msgstr "ਬਦਲੋ"

#: ../plugins/security-privacy/LockSecurity.qml:391
#: ../plugins/security-privacy/LockSecurity.qml:392
msgid "Set"
msgstr "ਸੈੱਟ"

#: ../plugins/security-privacy/LockSecurity.qml:463
#: ../plugins/security-privacy/LockSecurity.qml:464
#, fuzzy
msgid "Unlock the device using:"
msgstr "ਵਰਤੋਂ ਕਰਦੇ ਹੋਏ ਫ਼ੋਨ ਦਾ ਲਾਕ ਖੋਲ੍ਹੋ:"

#: ../plugins/security-privacy/LockSecurity.qml:467
#: ../plugins/security-privacy/LockSecurity.qml:468
msgid "Swipe (no security)"
msgstr "ਸਵਾਈਪ (ਬਿਨਾਂ ਸੁਰੱਖਿਆ)"

#: ../plugins/security-privacy/LockSecurity.qml:468
#: ../plugins/security-privacy/LockSecurity.qml:469
msgid "4-digit passcode"
msgstr "4-ਅੰਕ ਪਾਸਕੋਡ"

#: ../plugins/security-privacy/LockSecurity.qml:469
#: ../plugins/security-privacy/LockSecurity.qml:470
#: ../plugins/security-privacy/PhoneLocking.qml:62
#: ../plugins/security-privacy/PhoneLocking.qml:63
msgid "Passphrase"
msgstr "ਪਾਸਪਰੇਜ"

#: ../plugins/security-privacy/LockSecurity.qml:470
#: ../plugins/security-privacy/LockSecurity.qml:471
#: ../plugins/security-privacy/PhoneLocking.qml:63
#: ../plugins/security-privacy/PhoneLocking.qml:64
msgid "Fingerprint"
msgstr ""

#: ../plugins/security-privacy/LockSecurity.qml:471
#: ../plugins/security-privacy/LockSecurity.qml:472
msgid "Swipe (no security)… "
msgstr "ਸਵਾਈਪ (ਬਿਨਾਂ ਸੁਰੱਖਿਆ)... "

#: ../plugins/security-privacy/LockSecurity.qml:472
#: ../plugins/security-privacy/LockSecurity.qml:473
msgid "4-digit passcode…"
msgstr "4-ਅੰਕ ਪਾਸਕੋਡ..."

#: ../plugins/security-privacy/LockSecurity.qml:473
#: ../plugins/security-privacy/LockSecurity.qml:474
msgid "Passphrase…"
msgstr "ਪਾਸਪਰੇਜ..."

#: ../plugins/security-privacy/PageComponent.qml:39
#: ../plugins/security-privacy/PageComponent.qml:40
msgid "Security & Privacy"
msgstr "ਸੁਰੱਖਿਆ ਅਤੇ ਪਰਦੇਦਾਰੀ"

#: ../plugins/security-privacy/PageComponent.qml:125
#: ../plugins/security-privacy/PageComponent.qml:126
msgid "Security"
msgstr "ਸੁਰੱਖਿਆ"

#: ../plugins/security-privacy/PageComponent.qml:131
#: ../plugins/security-privacy/PageComponent.qml:132
msgid "Fingerprint ID"
msgstr ""

#: ../plugins/security-privacy/PageComponent.qml:151
#: ../plugins/security-privacy/PageComponent.qml:152
#: ../plugins/security-privacy/PhoneLocking.qml:32
#: ../plugins/security-privacy/PhoneLocking.qml:33
msgid "Locking and unlocking"
msgstr ""

#: ../plugins/security-privacy/PageComponent.qml:161
#: ../plugins/security-privacy/PageComponent.qml:162
#: ../plugins/security-privacy/SimPin.qml:37
#: ../plugins/security-privacy/SimPin.qml:398
msgid "SIM PIN"
msgstr "ਸਿਮ ਪਿਨ"

#: ../plugins/security-privacy/PageComponent.qml:164
#: ../plugins/security-privacy/PageComponent.qml:165
#: ../plugins/security-privacy/PageComponent.qml:236
#: ../plugins/security-privacy/PageComponent.qml:237
msgid "On"
msgstr "ਚਾਲੂ"

#: ../plugins/security-privacy/PageComponent.qml:168
#: ../plugins/security-privacy/PageComponent.qml:169
#: ../plugins/security-privacy/PageComponent.qml:236
#: ../plugins/security-privacy/PageComponent.qml:237
msgid "Off"
msgstr "ਬੰਦ"

#: ../plugins/security-privacy/PageComponent.qml:175
#: ../plugins/security-privacy/PageComponent.qml:176
msgid "Encryption"
msgstr "ਇਨਕ੍ਰਿਪਸ਼ਨ"

#: ../plugins/security-privacy/PageComponent.qml:185
#: ../plugins/security-privacy/PageComponent.qml:186
msgid ""
"Encryption protects against access to phone data when the phone is connected "
"to a PC or other device."
msgstr ""
"ਇਨਕ੍ਰਿਪਸ਼ਨ ਫ਼ੋਨ ਡਾਟੇ ਨੂੰ ਪਹੁੰਚ ਤੋਂ ਬਚਾਉਣ ਲਈ ਸੁਰੱਖਿਆ ਮਹੁੱਈਆ ਕਰਵਾਉਂਦੀ ਹੈ ਜਦੋਂ ਫ਼ੋਨ ਇੱਕ ਪੀਸੀ ਜਾਂ ਹੋਰ "
"ਜੰਤਰ ਨਾਲ ਜੁੜਿਆ ਹੋਵੇ।"

#: ../plugins/security-privacy/PageComponent.qml:190
#: ../plugins/security-privacy/PageComponent.qml:191
msgid "Privacy"
msgstr "ਪਰਦੇਦਾਰੀ"

#: ../plugins/security-privacy/PageComponent.qml:194
#: ../plugins/security-privacy/PageComponent.qml:195
msgid "Stats on welcome screen"
msgstr "ਸਵਾਗਤ ਸਕਰੀਨ ਤੇ ਅੰਕੜੇ"

#: ../plugins/security-privacy/PageComponent.qml:204
#: ../plugins/security-privacy/PageComponent.qml:205
msgid "Messages on welcome screen"
msgstr "ਸਵਾਗਤ ਸਕਰੀਨ ਤੇ ਸੁਨੇਹੇ"

#: ../plugins/security-privacy/PhoneLocking.qml:60
#: ../plugins/security-privacy/PhoneLocking.qml:61
msgctxt "Unlock with swipe"
msgid "None"
msgstr "ਕੋਈ ਨਹੀਂ"

#: ../plugins/security-privacy/PhoneLocking.qml:61
#: ../plugins/security-privacy/PhoneLocking.qml:62
msgid "Passcode"
msgstr "ਪਾਸਕੋਡ"

#: ../plugins/security-privacy/PhoneLocking.qml:89
#: ../plugins/security-privacy/PhoneLocking.qml:90
msgid "Lock when idle"
msgstr "ਵਿਹਲੇ ਹੋਣ ਤੇ ਲਾਕ"

#: ../plugins/security-privacy/PhoneLocking.qml:90
#: ../plugins/security-privacy/PhoneLocking.qml:91
msgid "Sleep when idle"
msgstr "ਵਿਹਲੇ ਹੋਣ ਤੇ ਨੀਂਦ"

#: ../plugins/security-privacy/PhoneLocking.qml:95
#: ../plugins/security-privacy/PhoneLocking.qml:96
#: ../plugins/security-privacy/PhoneLocking.qml:113
#: ../plugins/security-privacy/PhoneLocking.qml:114
msgid "Never"
msgstr "ਕਦੇ ਨਹੀਂ"

#. TRANSLATORS: %1 is the number of seconds
#: ../plugins/security-privacy/PhoneLocking.qml:98
#: ../plugins/security-privacy/PhoneLocking.qml:99
#, fuzzy, qt-format
msgid "After %1 second"
msgid_plural "After %1 seconds"
msgstr[0] "%1 ਮਿੰਟ ਬਾਅਦ"
msgstr[1] "%1 ਮਿੰਟਾਂ ਬਾਅਦ"

#. TRANSLATORS: %1 is the number of minutes
#: ../plugins/security-privacy/PhoneLocking.qml:102
#: ../plugins/security-privacy/PhoneLocking.qml:103
#: ../plugins/security-privacy/PhoneLocking.qml:110
#: ../plugins/security-privacy/PhoneLocking.qml:111
#, qt-format
msgid "After %1 minute"
msgid_plural "After %1 minutes"
msgstr[0] "%1 ਮਿੰਟ ਬਾਅਦ"
msgstr[1] "%1 ਮਿੰਟਾਂ ਬਾਅਦ"

#: ../plugins/security-privacy/PhoneLocking.qml:126
#: ../plugins/security-privacy/PhoneLocking.qml:127
msgid "Sleep locks immediately"
msgstr "ਨੀਂਦ ਲਾਕ ਹੁਣੇ"

#: ../plugins/security-privacy/PhoneLocking.qml:132
#: ../plugins/security-privacy/PhoneLocking.qml:133
msgid "When locked, allow:"
msgstr "ਜਦੋਂ ਲਾਕ ਹੋਇਆ, ਪ੍ਰਵਾਨ:"

#: ../plugins/security-privacy/PhoneLocking.qml:138
#: ../plugins/security-privacy/PhoneLocking.qml:139
msgid "Launcher"
msgstr "ਲਾਂਚਰ"

#: ../plugins/security-privacy/PhoneLocking.qml:152
#: ../plugins/security-privacy/PhoneLocking.qml:153
msgid "Notifications and quick settings"
msgstr "ਸੂਚਨਾ ਅਤੇ ਜਲਦੀ ਸੈਟਿੰਗਾਂ"

#: ../plugins/security-privacy/PhoneLocking.qml:167
#: ../plugins/security-privacy/PhoneLocking.qml:168
#, fuzzy
msgid "Turn on lock security to restrict access when the device is locked."
msgstr "ਪਹੁੰਚ ਤੇ ਪਾਬੰਦੀ ਲਾਉਣ ਲਈ ਲਾਕ ਸੁਰੱਖਿਆ ਚਾਲੂ ਕਰੋ ਜਦੋਂ ਫ਼ੋਨ ਲਾਕ ਹੋਵੇ।"

#: ../plugins/security-privacy/PhoneLocking.qml:168
#: ../plugins/security-privacy/PhoneLocking.qml:169
msgid "Other apps and functions will prompt you to unlock."
msgstr "ਹੋਰ ਐਪ ਅਤੇ ਕਾਰਜ ਤੁਹਾਨੂੰ ਅਣਲਾਕ ਕਰਨ ਲਈ ਕਹਿਣਗੇ।"

#: ../plugins/security-privacy/SimPin.qml:48
msgid "Change SIM PIN"
msgstr "ਸਿਮ ਪਿਨ ਬਦਲੋ"

#: ../plugins/security-privacy/SimPin.qml:53
#: ../plugins/security-privacy/SimPin.qml:219
#, qt-format
msgid "Incorrect PIN. %1 attempt remaining."
msgid_plural "Incorrect PIN. %1 attempts remaining."
msgstr[0] "ਗਲਤ ਪਿਨ। %1 ਕੋਸ਼ਿਸ ਬਾਕੀ।"
msgstr[1] "ਗਲਤ ਪਿਨ। %1 ਕੋਸ਼ਿਸਾਂ ਬਾਕੀ।"

#: ../plugins/security-privacy/SimPin.qml:58
#: ../plugins/security-privacy/SimPin.qml:120
#: ../plugins/security-privacy/SimPin.qml:224
#: ../plugins/security-privacy/SimPin.qml:297
#, fuzzy
msgid "No more attempts allowed"
msgstr "%1 ਕੋਸ਼ਿਸਾਂ ਦੀ ਇਜਾਜਤ"

#: ../plugins/security-privacy/SimPin.qml:93
msgid "Current PIN:"
msgstr "ਮੌਜੂਦਾ ਪਿਨ:"

#: ../plugins/security-privacy/SimPin.qml:115
#: ../plugins/security-privacy/SimPin.qml:292
#, qt-format
msgid "%1 attempt allowed."
msgid_plural "%1 attempts allowed."
msgstr[0] "%1 ਕੋਸ਼ਿਸ ਦੀ ਇਜਾਜ਼ਤ।"
msgstr[1] "%1 ਕੋਸ਼ਿਸਾਂ ਦੀ ਇਜਾਜ਼ਤ।"

#: ../plugins/security-privacy/SimPin.qml:135
msgid "Choose new PIN:"
msgstr "ਨਵੀਂ ਪਿਨ ਚੁਣੋ:"

#: ../plugins/security-privacy/SimPin.qml:146
msgid "Confirm new PIN:"
msgstr "ਨਵੇਂ ਪਿਨ ਦੀ ਪੁਸ਼ਟੀ ਕਰੋ:"

#: ../plugins/security-privacy/SimPin.qml:168
msgid "PINs don't match. Try again."
msgstr "ਪਿਨਾਂ ਦਾ ਮੇਲ ਨਹੀਂ, ਮੁੜ-ਕੋਸ਼ਿਸ ਕਰੋ।"

#: ../plugins/security-privacy/SimPin.qml:213
msgid "Enter SIM PIN"
msgstr "ਸਿਮ ਪਿਨ ਦਰਜ ਕਰੋ"

#: ../plugins/security-privacy/SimPin.qml:214
msgid "Enter Previous SIM PIN"
msgstr "ਪਿਛਲਾ ਸਿਮ ਪਿਨ ਦਰਜ ਕਰੋ"

#: ../plugins/security-privacy/SimPin.qml:334
msgid "Lock"
msgstr "ਲਾਕ ਕਰੋ"

#: ../plugins/security-privacy/SimPin.qml:334
msgid "Unlock"
msgstr "ਅਣ-ਲਾਕ"

#: ../plugins/security-privacy/SimPin.qml:413
#, fuzzy
msgid "Unlocked"
msgstr "ਅਣ-ਲਾਕ"

#: ../plugins/security-privacy/SimPin.qml:416
msgid "Change PIN…"
msgstr "ਪਿਨ ਬਦਲੋ..."

#: ../plugins/security-privacy/SimPin.qml:427
#, fuzzy
msgid "Locked"
msgstr "ਲਾਕ ਕਰੋ"

#: ../plugins/security-privacy/SimPin.qml:431
#, fuzzy
msgid "Unlock…"
msgstr "ਅਣ-ਲਾਕ"

#: ../plugins/security-privacy/SimPin.qml:445
#, fuzzy
msgid ""
"When a SIM PIN is set, it must be entered to access cellular services after "
"restarting the device or swapping the SIM."
msgstr ""
"ਜਦੋਂ ਇੱਕ ਸਿਮ ਪਿਨ ਸੈੱਟ ਹੈ, ਤਾਂ ਇਸ ਨੂੰ ਫ਼ੋਨ ਮੁੜ-ਚਾਲੂ ਕਰਨ ਤੋਂ ਬਾਅਦ ਸੈਲੂਲਰ ਸੇਵਾਵਾਂ ਤੇ ਪਹੁੰਚ ਲਈ ਅਤੇ ਸਿਮ "
"ਦੀ ਅਦਲਾ-ਬਦਲੀ ਸਮੇਂ ਦਾਖਲ ਕਰਨਾ ਚਾਹੀਦਾ।"

#: ../plugins/security-privacy/SimPin.qml:449
msgid "Entering an incorrect PIN repeatedly may lock the SIM permanently."
msgstr "ਇੱਕ ਗਲਤ ਪਿਨ ਵਾਰ-ਵਾਰ ਦਾਖਲ ਕਰਨਾ ਸਿਮ ਨੂੰ ਪੱਕੇ ਤੌਰ ਤੇ ਲਾਕ ਕਰ ਸਕਦਾ।"

#: ../plugins/security-privacy/here-terms.qml:24
#: ../plugins/security-privacy/here-terms.qml:25
msgid "Nokia HERE"
msgstr ""

#: ../plugins/security-privacy/securityprivacy.cpp:369
msgid "Incorrect passcode. Try again."
msgstr "ਗਲਤ ਪਾਸਕੋਡ। ਮੁੜ ਕੋਸਿਸ ਕਰੋ।"

#: ../plugins/security-privacy/securityprivacy.cpp:371
msgid "Incorrect passphrase. Try again."
msgstr "ਗਲਤ ਵਾਕ ਹੈ। ਮੁੜ ਕੋਸ਼ਿਸ਼ ਕਰੋ।"

#: ../plugins/security-privacy/securityprivacy.cpp:374
msgid "Could not set security mode"
msgstr "ਸੁਰੱਖਿਆ ਮੋਡ ਸੈੱਟ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਿਆ"

#: ../plugins/security-privacy/securityprivacy.cpp:399
msgid "Could not set security display hint"
msgstr "ਸੁਰੱਖਿਆ ਡਿਸਪਲੇਅ ਇਸ਼ਾਰਾ ਸੈੱਟ ਨਹੀਂ ਕਰ ਸਕਿਆ"

#: ../plugins/security-privacy/securityprivacy.cpp:413
msgid "Authentication token manipulation error"
msgstr "ਪ੍ਰਮਾਣਿਕਤਾ ਟੋਕਨ ਸੋਧ ਗਲਤੀ"

#~ msgid "Phone and Internet"
#~ msgstr "ਫ਼ੋਨ ਅਤੇ ਇੰਟਰਨੈੱਟ"

#~ msgid "Phone only"
#~ msgstr "ਕੇਵਲ ਫ਼ੋਨ"

#~ msgid "lock"
#~ msgstr "ਲਾਕ"

#~ msgid "sim"
#~ msgstr "ਸਿਮ"

#~ msgid "security"
#~ msgstr "ਸੁਰੱਖਿਆ"

#~ msgid "privacy"
#~ msgstr "ਪਰਦੇਦਾਰੀ"

#~ msgid "pin"
#~ msgstr "ਪਿਨ"

#~ msgid "code"
#~ msgstr "ਕੋਡ"

#~ msgid "password"
#~ msgstr "ਪਾਸਵਰਡ"

#~ msgid "passphrase"
#~ msgstr "ਵਾਕ"

#~ msgid "swipe"
#~ msgstr "ਘਸੀਟੋ"

#~ msgid "allow"
#~ msgstr "ਪ੍ਰਵਾਨ"

#~ msgid "access"
#~ msgstr "ਪਹੁੰਚ"
